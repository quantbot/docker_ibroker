# This Dockerfile is used to build an sakuli image based on Centos

FROM centos:7.2.1511

MAINTAINER Tobias Schneck "tobias.schneck@consol.de"
ENV REFRESHED_AT 2015-12-18


ENV DISPLAY :1
ENV VNC_COL_DEPTH 16
ENV VNC_RESOLUTION 1280x800
ENV VNC_PW vncpasswd


############### xvnc / xfce installation
RUN yum -y install epel-release; yum clean all
RUN yum -y update; yum clean all
RUN yum -y install sudo
RUN yum --enablerepo=epel -y -x gnome-keyring --skip-broken groups install "Xfce"
# RUN yum -y groups install "Fonts"
RUN yum -y install tigervnc-server wget net-tools unzip 
### Install noVNC - HTML5 based VNC viewer
#RUN mkdir -p $NO_VNC_HOME/utils/websockify \
#    && wget -qO- https://github.com/kanaka/noVNC/archive/master.tar.gz | tar xz --strip 1 -C $NO_VNC_HOME \
#    &&  wget -qO- https://github.com/kanaka/websockify/archive/v0.7.0.tar.gz | tar xz --strip 1 -C $NO_VNC_HOME/utils/websockify \
#    && chmod +x -v /root/noVNC/utils/*.sh

### Install java and java-plugin
# RUN yum -y install $SAKULI_DOWNLOAD_URL/3rd-party/java/jre-$JAVA_VERSION-linux-x64.rpm
# creat symbolic link for firefox java plugin
# RUN ln -s /usr/java/default/lib/amd64/libnpjp2.so /usr/lib64/mozilla/plugins/

### Install chrome browser
#ADD repos /etc/yum.repos.d/
#RUN yum -y install google-chrome-stable \
#    && echo "alias google-chrome='/usr/bin/google-chrome --user-data-dir'" >> /root/.bashrc \
#    && echo "alias google-chrome='/usr/bin/google-chrome -no-default-browser-check --user-data-dir'" >> /etc/bashrc

# xvnc server porst, if $DISPLAY=:1 port will be 5901
EXPOSE 5901
# novnc web port
# EXPOSE 6901

ADD .vnc /root/.vnc
ADD .config /root/.config
ADD Desktop /root/Desktop
ADD scripts /root/scripts
RUN chmod +x  /root/scripts/*.sh /root/.vnc/xstartup /etc/xdg/xfce4/xinitrc 
RUN /bin/dbus-uuidgen > /etc/machine-id
RUN wget https://download2.interactivebrokers.com/installers/tws/latest-standalone/tws-latest-standalone-linux-x64.sh \
    && chmod +x tws-latest-standalone-linux-x64.sh \
    && ./tws-latest-standalone-linux-x64.sh  -q \
    && rm tws-latest-standalone-linux-x64.sh
RUN wget https://download2.interactivebrokers.com/installers/ibgateway/latest-standalone/ibgateway-latest-standalone-linux-x64.sh \
    && chmod +x ibgateway-latest-standalone-linux-x64.sh \
    && ./ibgateway-latest-standalone-linux-x64.sh -q \
    && rm ibgateway-latest-standalone-linux-x64.sh
ENTRYPOINT ["/root/scripts/vnc_startup.sh"]
CMD ["--tail-log"]
EXPOSE 7496 4001
VOLUME /IBVar
